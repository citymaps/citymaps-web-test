import React from 'react';

import Business from './Business';

export default class IndexPage extends React.Component {

  constructor(props) {
    super();
  }

  render() {
    return (
      <html>
      <head>
        <title>Citymaps</title>
      </head>
      <body>
        <div className="root">
          <h1>Hello React</h1>
          <Business bid={ this.props.bid } />
        </div>
      </body>
      </html>
    );
  }

}