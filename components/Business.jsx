import React from 'react';

export default class Business extends React.Component {

  render() {
    return (
      <div className="business">
        <h2>Business ID: { this.props.bid }</h2>
      </div>
    );
  }

}