// ES6 shim file. All files required/imported after this point may use ES6 syntax.
// For more information see: https://babeljs.io/docs/usage/require/

require("babel-core/register");
require("./server.js").start();