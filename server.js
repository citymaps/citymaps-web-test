import express from 'express';
import React   from 'react';
import ReactDOMServer from 'react-dom/server'

let PORT = process.env.PORT || 9615;
let server = express();

import IndexPage from './components/index';


// Express route to retrieve a business.
// For more information see: https://expressjs.com/en/guide/routing.html
server.get('/business/:bid', (req, res) => {
  var html = ReactDOMServer.renderToString(<IndexPage bid={ req.params.bid } />);
  res.write(html);
  res.end();
});

// Default route
server.get('/', (req,res) => {
  res.write("Hello Express");
  res.end();
});


module.exports = {
  start: () => {
    console.log("Server listening on port %d", PORT);
    server.listen(PORT);
  }
};