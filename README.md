Citymaps Web code test
======================

The Citymaps Business API fetches a business using its unique ID:
    https://coreapi.citymaps.com/v2/business/a9dc01e3-5191-4673-a408-ece8562c7ff9?crosslinks=true

In addition to basic business information, the "crosslinks" parameter will fetch "related businesses" for a given venue.

Modify this application to retrieve a Citymaps business using the above endpoint.
In the HTML response, display some information on the business (name, address, phone, website, an image)
and its list of related businesses, providing links to each. When complete, a web-browsing user
should be able to "crawl" through an indefinite number of businesses by following crosslinks.

Provide a simple CSS style served alongside the HTML content. Ensure that you are separating your UI into React components as necessary.

To get you started, a boilerplate server-side implementation of Babel and React is provided.


Getting started
---------------

Clone the repository.
    cd citymaps-web-test

Install dependencies (React, Express)
    npm install

Start the Express server.
    npm start

In the browser, navigate to:
    http://localhost:9615
    http://localhost:9615/business/foo
